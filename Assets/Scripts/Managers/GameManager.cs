﻿using System;
using System.Collections;
using UnityEngine;

public class GameManager : IDisposable
{
    public References References { get; } = new References();
    public Player Player { get; } = new Player();


    private readonly UIDialogs _uiDialogs;

    public GameManager(UIDialogs uiDialogs)
    {
        _uiDialogs = uiDialogs;
    }

    public IEnumerator InitAsync()
    {
        Debug.Log("[GameManager] Init");

        yield return References.InitAsync();
        Player.Init(References);

        _uiDialogs.Init(this);

        yield return null; // Wait for init plugins and other dependencies.

        Debug.Log("[GameManager] Init complete");
    }

    public void Dispose()
    {
        Player?.Dispose();
    }
}