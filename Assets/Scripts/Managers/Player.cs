﻿using System;
using UnityEngine;

public class Player : IDisposable
{
    public Skills Skills { get; private set; }

    public ISkillPointWallet SkillPoints => _skillPoints;

    private readonly SkillDataPlayerPrefs _skillData = new SkillDataPlayerPrefs();
    private readonly SkillPointWalletPlayerPrefs _skillPoints = new SkillPointWalletPlayerPrefs();

    public void Init(References references)
    {
        Debug.Log("[Player] Init");

        _skillPoints.Init(references.SkillTree);

        Skills = new Skills(_skillData, references.SkillTree, _skillPoints);
        Debug.Log($"[Player] Skill Tree:\n{Skills.PrintTree()}");

        Debug.Log("[Player] Init complete");

        _skillPoints.Subscribe(_skillData);
    }

    public void Dispose()
    {
        _skillPoints.Unsubscribe(_skillData);
    }
}