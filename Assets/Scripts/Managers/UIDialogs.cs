﻿using UnityEngine;

public class UIDialogs : MonoBehaviour
{
    [SerializeField] private SkillTreeDialog _skillTree;
    [SerializeField] private SkillPointsView _skillPointsView;
    [SerializeField] private AddSkillPointsButton _addSkillPointsButton;
    [SerializeField] private SubSkillPointsButton _subSkillPointsButton;


    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void Init(GameManager gameManager)
    {
        _skillTree.Init(gameManager);
        _skillPointsView.Init(gameManager.Player.SkillPoints);
        _addSkillPointsButton.Init(gameManager.Player.SkillPoints);
        _subSkillPointsButton.Init(gameManager.Player.SkillPoints);
    }
}