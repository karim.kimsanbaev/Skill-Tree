﻿using System.Collections;

public class References
{
    public ISkillTreeDescription SkillTree { get; } = new SkillTreeDescriptionSoResource();

    public IEnumerator InitAsync()
    {
        yield return SkillTree.InitAsync();
    }
}