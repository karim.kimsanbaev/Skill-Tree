﻿using System;
using System.Linq;
using Hierarchy;

namespace Extensions.GraphExtensions
{
    public static class GraphNodeReachability
    {
        /// <summary>
        /// Can reach node by node with condition.
        /// </summary>
        public static bool IsReached<TData>(this IGraphNode<TData> node, Func<IGraphNode<TData>, bool> comparer,
            SearchOptions<TData> searchOptions,
            TraversalType traversalType = TraversalType.BreadthFirst)
        {
            foreach (var currentNode in node.Search(searchOptions, traversalType))
            {
                if (currentNode.NeighborNodes().Any(comparer))
                {
                    return true;
                }
            }

            return false;
        }
    }
}