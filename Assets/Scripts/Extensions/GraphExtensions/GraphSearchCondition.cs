﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hierarchy;

namespace Extensions.GraphExtensions
{
    public delegate bool SearchOptions<TData>(IGraphNode<TData> node);

    public static class GraphSearchCondition
    {
        public static IEnumerable<IGraphNode<TData>> Search<TData>(this IGraphNode<TData> node,
            SearchOptions<TData> options, TraversalType traversalType = TraversalType.BreadthFirst)
        {
            switch (traversalType)
            {
                case TraversalType.BreadthFirst:
                    return node.BreadthSearch(options);
                default:
                    return null;
            }
        }

        private static IEnumerable<IGraphNode<TData>> NeighborNodes<TData>(this IGraphNode<TData> node,
            SearchOptions<TData> options)
        {
            var comparer = new Func<IGraphNode<TData>, bool>(options);
            return node.Children.Where(comparer).Concat(node.Parents.Where(comparer));
        }

        private static IEnumerable<IGraphNode<TData>> BreadthSearch<TData>(this IGraphNode<TData> node,
            SearchOptions<TData> options)
        {
            return node.BreadthFirstSearch(n => n.NeighborNodes(options));
        }
    }
}