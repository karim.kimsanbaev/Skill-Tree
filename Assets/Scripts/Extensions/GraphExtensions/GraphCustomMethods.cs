﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hierarchy;

namespace Extensions.GraphExtensions
{
    public static class GraphCustomMethods
    {
        /// <summary>
        /// Like origin AddChild, but return child node.
        /// </summary>
        public static TGraphNode AddChild<TGraphNode, TData>(this TGraphNode sourceNode, TData childData)
            where TGraphNode : IGraphNode<TData>, new()
        {
            var childNode = new TGraphNode() {Data = childData, Parents = new List<IGraphNode<TData>>() {sourceNode}};
            sourceNode.Children.Add(childNode);
            return childNode;
        }

        /// <summary>
        /// Print Tree Extension like Hierarchy
        /// </summary>
        public static string PrintTree<TData>(this IGraphNode<TData> rootNode, int level = 0,
            string indenter = "│   ", int indentSize = 0, StringBuilder sb = null)
        {
            if (sb == null)
            {
                sb = new StringBuilder();
                indentSize = indenter.Length;
            }

            sb.AppendLine(rootNode.Data.ToString());
            return rootNode.Children.PrintTree(level + 1, indenter, indentSize, sb);
        }

        private static string PrintTree<TData>(this IEnumerable<IGraphNode<TData>> sourceNodes, int level = 0,
            string indenter = "│   ", int indentSize = 0, StringBuilder sb = null)
        {
            if (sb == null)
            {
                sb = new StringBuilder();
                indentSize = indenter.Length;
            }

            foreach (var node in sourceNodes)
            {
                if (level > 0)
                {
                    if (level > 1)
                    {
                        sb.Append(indenter);
                    }

                    sb.AppendLine(node.Parents.First().Children.Last() == node ? $"└─ {node}" : $"├─ {node}");
                }

                node.Children.PrintTree(level + 1, "    ", indentSize, sb);
            }

            return sb.ToString();
        }
    }
}