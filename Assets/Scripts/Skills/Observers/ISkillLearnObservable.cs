﻿using System;

public interface ISkillLearnObservable
{
    event Action<string> SkillLearned;
    event Action<string> SkillForgotten;
}