﻿internal interface ISkillInternal : ISkill
{
    void Learn();
    void Forget();
    bool CanLearn { get; }
    bool CanForget { get; }
}