﻿internal class Skill : ISkillInternal
{
    public string Id { get; }
    public bool IsLearned => _skillData.IsLearned(Id);

    private readonly ISkillData _skillData;

    public static ISkillInternal Create(ISkillDescription skillDescription, ISkillData skillData)
    {
        if (skillDescription.Id == Constants.BaseSkill)
        {
            return new BaseSkill();
        }

        return new Skill(skillDescription, skillData);
    }

    private Skill(ISkillDescription skillDescription, ISkillData skillData)
    {
        _skillData = skillData;
        Id = skillDescription.Id;
    }

    public override string ToString()
    {
        return Id;
    }

    void ISkillInternal.Learn()
    {
        _skillData.Learn(Id);
    }

    void ISkillInternal.Forget()
    {
        _skillData.Forget(Id);
    }

    public bool CanLearn => IsLearned == false;
    public bool CanForget => IsLearned == true;
}