﻿internal class BaseSkill : ISkillInternal
{
    public string Id => Constants.BaseSkill;
    public bool IsLearned => true;

    void ISkillInternal.Learn()
    {
    }

    void ISkillInternal.Forget()
    {
    }

    public bool CanLearn => false;
    public bool CanForget => false;
}