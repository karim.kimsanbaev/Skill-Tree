﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Extensions.GraphExtensions;
using Hierarchy;

public class Skills : IReadOnlyCollection<ISkill>
{
    private readonly ISkillTreeDescription _skillTreeDescription;
    private readonly ISkillPointWallet _wallet;
    private readonly SkillCollection _skillCollection;

    public Skills(ISkillData skillData, ISkillTreeDescription skillTreeDescription, ISkillPointWallet wallet)
    {
        _skillTreeDescription = skillTreeDescription;
        _wallet = wallet;
        _skillCollection = new SkillCollection(skillData, skillTreeDescription);
    }

    public IEnumerator<ISkill> GetEnumerator()
    {
        return _skillCollection.ToFlatDataList().GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public ISkill this[string id] => _skillCollection[id];

    public int Count => _skillCollection.ToFlatDataList().Count();

    public bool CanLearn(string skillId)
    {
        var isReached = _skillCollection.IsReached(node => node.Data.Id == skillId, node => node.Data.IsLearned);
        var canLearn = _skillCollection[skillId].CanLearn;
        var canPaid = _wallet.Total >= _skillTreeDescription[skillId].Price;

        return isReached && canLearn && canPaid;
    }

    public bool CanForget(string skillId)
    {
        var isReached = _skillCollection.IsReached(node => node.Data.Id == skillId, node => node.Data.IsLearned);
        var canForget = _skillCollection[skillId].CanForget;
        var isReachedAllLearnedChilds = _skillCollection.IsReachedAllLearnedChildsWithoutSkill(skillId);

        return isReached && canForget && isReachedAllLearnedChilds;
    }

    public void Learn(string skillId)
    {
        if (CanLearn(skillId))
        {
            _skillCollection[skillId].Learn();
        }
    }

    public void Forget(string skillId)
    {
        if (CanForget(skillId))
        {
            _skillCollection[skillId].Forget();
        }
    }

    public void ForgetAll()
    {
        foreach (var skill in _skillCollection.ToFlatDataList())
        {
            skill.Forget();
        }
    }

    public string PrintTree() => _skillCollection.PrintTree();
}