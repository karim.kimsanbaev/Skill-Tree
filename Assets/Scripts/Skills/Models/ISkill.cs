﻿public interface ISkill
{
    string Id { get; }
    bool IsLearned { get; }
}