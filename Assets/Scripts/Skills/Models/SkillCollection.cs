﻿using System.Collections.Generic;
using System.Linq;
using Extensions.GraphExtensions;
using Hierarchy;

internal class SkillCollection : IGraphNode<ISkillInternal>
{
    private readonly GraphNode<ISkillInternal> _skills;

    public SkillCollection(ISkillData skillData, ISkillTreeDescription skillTreeDescription)
    {
        _skills = skillTreeDescription
            .Select(x => Skill.Create(x.Value, skillData))
            .ToGraph(idSelector => idSelector.Id,
                parentSelector => skillTreeDescription.GetParentIds(parentSelector.Id))
            .First(skill => skill.Data.Id == Constants.BaseSkill);
    }

    public bool IsReachedAllLearnedChildsWithoutSkill(string skillId)
    {
        var skillNode = _skills.Search().First(node => node.Data.Id == skillId);
        foreach (var childNode in skillNode.Children)
        {
            if (!childNode.Data.IsLearned)
            {
                continue;
            }

            // Может ли дочерний узел добраться до базового узла, если идти только по выученым нодам И выключаем текущую ноду из поиска.
            var canReachBaseNode = childNode.IsReached(node => node.Data.Id == Constants.BaseSkill,
                node => node.Data.IsLearned && node.Data.Id != skillId);
            if (!canReachBaseNode)
            {
                return false;
            }
        }

        return true;
    }

    public IList<IGraphNode<ISkillInternal>> Parents
    {
        get => _skills.Parents;
        set => _skills.Parents = value;
    }

    public IList<IGraphNode<ISkillInternal>> Children
    {
        get => _skills.Children;
        set => _skills.Children = value;
    }

    public ISkillInternal Data
    {
        get => _skills.Data;
        set => _skills.Data = value;
    }

    public ISkillInternal this[string id] => _skills.Search().First(node => node.Data.Id == id).Data;
}