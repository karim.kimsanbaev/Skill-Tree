﻿using System;
using UnityEngine;

public class SkillDataPlayerPrefs : ISkillData, ISkillLearnObservable
{
    private const string LearnKeyPostfix = "_learn";

    public bool IsLearned(string id)
    {
        return PlayerPrefs.HasKey(GetLearnKeyFor(id));
    }

    public void Learn(string id)
    {
        Debug.Log($"[SkillDataPlayerPrefs] Learn {id}");
        
        var key = GetLearnKeyFor(id);
        PlayerPrefs.SetInt(key, 1);

        if (PlayerPrefs.HasKey(key))
        {
            SkillLearned?.Invoke(id);
        }
    }

    public void Forget(string id)
    {
        Debug.Log($"[SkillDataPlayerPrefs] Forget {id}");

        var key = GetLearnKeyFor(id);
        PlayerPrefs.DeleteKey(GetLearnKeyFor(id));

        if (!PlayerPrefs.HasKey(key))
        {
            SkillForgotten?.Invoke(id);
        }
    }

    private string GetLearnKeyFor(string id)
    {
        return $"{id}{LearnKeyPostfix}";
    }

    public event Action<string> SkillLearned;
    public event Action<string> SkillForgotten;
}