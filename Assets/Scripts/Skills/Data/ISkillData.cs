﻿using System;

public interface ISkillData
{
    bool IsLearned(string id);
    void Learn(string id);
    void Forget(string id);
}