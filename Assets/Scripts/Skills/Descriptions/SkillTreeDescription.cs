﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SkillTreeDescriptionSoResource : ISkillTreeDescription
{
    private Dictionary<string, ISkillDescription> _descriptions;

    public IEnumerator InitAsync()
    {
        // TODO: Use Asset Bundle here.
        yield return null;
        var skillDescriptions = Resources.LoadAll<SkillDescriptionSo>(string.Empty);
        _descriptions = skillDescriptions.ToDictionary(desc => desc.Id, desc => (ISkillDescription) desc);
    }

    public IEnumerable<string> GetParentIds(string skillId)
    {
        foreach (var description in _descriptions.Values)
        {
            if (description.ChildIds.Contains(skillId))
            {
                yield return description.Id;
            }
        }
    }

    #region | Delegate IReadOnlyDictionary |

    public IEnumerator<KeyValuePair<string, ISkillDescription>> GetEnumerator()
    {
        return _descriptions.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable) _descriptions).GetEnumerator();
    }

    public int Count => _descriptions.Count;

    public bool ContainsKey(string key)
    {
        return _descriptions.ContainsKey(key);
    }

    public bool TryGetValue(string key, out ISkillDescription value)
    {
        return _descriptions.TryGetValue(key, out value);
    }

    public ISkillDescription this[string key] => _descriptions[key];

    public IEnumerable<string> Keys => ((IReadOnlyDictionary<string, ISkillDescription>) _descriptions).Keys;

    public IEnumerable<ISkillDescription> Values =>
        ((IReadOnlyDictionary<string, ISkillDescription>) _descriptions).Values;

    #endregion
}