﻿using System.Collections.Generic;

public interface ISkillDescription
{
    string Id { get; }
    HashSet<string> ChildIds { get; }

    string Title { get; }
    string Description { get; }
    SkillPoint Price { get; }
}