﻿using System.Collections;
using System.Collections.Generic;

public interface ISkillTreeDescription : IReadOnlyDictionary<string, ISkillDescription>
{
    IEnumerator InitAsync();
    IEnumerable<string> GetParentIds(string skillId);
}