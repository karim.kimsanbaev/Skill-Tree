﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillDescription", menuName = "ScriptableObjects/Skill/Description", order = 1)]
public class SkillDescriptionSo : ScriptableObject, ISkillDescription
{
    [SerializeField] private string _id;

    [SerializeField] private SkillDescriptionSo[] _childs;

    [SerializeField] private string _title;
    [Multiline] [SerializeField] private string _description;
    [SerializeField] private SkillPoint _price;

    public string Id => _id;

    public HashSet<string> ChildIds =>
        new HashSet<string>(_childs?.Select(desc => desc.Id) ?? Enumerable.Empty<string>());

    public string Title => _title;

    public string Description => _description;
    public SkillPoint Price => _price;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _id = name;
    }
#endif
}