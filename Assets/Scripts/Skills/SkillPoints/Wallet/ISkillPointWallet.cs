﻿using System;

public interface ISkillPointWallet
{
    SkillPoint Total { get; }
    void AddPoint();
    void SubPoint();

    event Action<SkillPoint> Changed;
}