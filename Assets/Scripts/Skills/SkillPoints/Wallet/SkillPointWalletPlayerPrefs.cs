﻿using System;
using UnityEngine;

public class SkillPointWalletPlayerPrefs : ISkillPointWallet
{
    private ISkillTreeDescription _skillTreeDescription;
    private const string SkillPointKey = "skill_point_total";

    public SkillPoint Total => GetTotal();

    public void Init(ISkillTreeDescription skillTreeDescription)
    {
        _skillTreeDescription = skillTreeDescription;
    }

    public void Subscribe(ISkillLearnObservable observable)
    {
        observable.SkillLearned += OnSkillLearn;
        observable.SkillForgotten += OnSkillForgotten;
    }

    public void Unsubscribe(ISkillLearnObservable observable)
    {
        observable.SkillLearned -= OnSkillLearn;
        observable.SkillForgotten -= OnSkillForgotten;
    }

    private void OnSkillLearn(string skillId)
    {
        var price = _skillTreeDescription[skillId].Price;

        var newTotal = Total - price;
        SetTotal(newTotal);

        Debug.Log($"[SkillPointWalletPlayerPrefs] Paid skill '{skillId}' for '{price}'");
    }

    private void OnSkillForgotten(string skillId)
    {
        var price = _skillTreeDescription[skillId].Price;
        var newTotal = Total + price;
        SetTotal(newTotal);

        Debug.Log($"[SkillPointWalletPlayerPrefs] Forgot skill '{skillId}' for '{price}'");
    }

    public void AddPoint()
    {
        SetTotal(Total + 1);
        Debug.Log($"[SkillPointWalletPlayerPrefs] Add 1 skill point");
    }

    public void SubPoint()
    {
        SetTotal(Total - 1);
        Debug.Log($"[SkillPointWalletPlayerPrefs] Add 1 skill point");
    }

    public event Action<SkillPoint> Changed;

    private SkillPoint GetTotal()
    {
        return new SkillPoint(PlayerPrefs.GetInt(SkillPointKey));
    }

    private void SetTotal(SkillPoint value)
    {
        PlayerPrefs.SetInt(SkillPointKey, value.Value);
        Changed?.Invoke(value);
    }
}