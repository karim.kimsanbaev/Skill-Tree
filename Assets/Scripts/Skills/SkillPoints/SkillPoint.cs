﻿using System;
using UnityEngine;

[Serializable]
public struct SkillPoint : IEquatable<SkillPoint>
{
    [SerializeField] private int value;

    public int Value => value;

    public SkillPoint(int value)
    {
        this.value = Math.Max(0, value);
    }

    public override string ToString()
    {
        return $"{value} sp";
    }

    public bool Equals(SkillPoint other)
    {
        return value == other.value;
    }

    public override bool Equals(object obj)
    {
        return obj is SkillPoint other && Equals(other);
    }

    public override int GetHashCode()
    {
        return value;
    }

    public static implicit operator SkillPoint(int value)
    {
        return new SkillPoint(value);
    }

    public static implicit operator int(SkillPoint value)
    {
        return value.value;
    }
}