using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillDescriptionView : UIBehaviour
{
    [SerializeField] private Text _title;
    [SerializeField] private Text _description;
    [SerializeField] private Text _price;

    private ISkillTreeDescription _skillTreeDescription;

    public void Init(ISkillTreeDescription skillTreeDescription)
    {
        _skillTreeDescription = skillTreeDescription;
    }

    public void SetDescription(string id)
    {
        var description = _skillTreeDescription[id];
        _title.text = description.Title;
        _description.text = description.Description;
        _price.text = $"Price {description.Price}";
    }
}