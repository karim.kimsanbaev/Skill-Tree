using UnityEngine.EventSystems;

public class AddSkillPointsButton : UIBehaviour, IPointerClickHandler
{
    private ISkillPointWallet _wallet;

    public void Init(ISkillPointWallet wallet)
    {
        _wallet = wallet;
    }

    public void OnPointerClick(PointerEventData eventData) => _wallet.AddPoint();
}