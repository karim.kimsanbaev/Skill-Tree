using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillPointsView : UIBehaviour
{
    [SerializeField] private Text _text;

    private ISkillPointWallet _wallet;
    private SkillPoint? _lastPoints;

    public void Init(ISkillPointWallet wallet)
    {
        _wallet = wallet;
    }

    private void Update()
    {
        if (_wallet == null)
        {
            _text.text = "Skill Points: N/A";
            return;
        }

        if (_lastPoints != null && _lastPoints.Equals(_wallet.Total))
        {
            return;
        }

        _text.text = $"Skill Points: {_wallet.Total}";
        _lastPoints = _wallet.Total;
    }
}