﻿using UnityEngine.EventSystems;

public class SubSkillPointsButton : UIBehaviour, IPointerClickHandler
{
    private ISkillPointWallet _wallet;

    public void Init(ISkillPointWallet wallet)
    {
        _wallet = wallet;
    }

    public void OnPointerClick(PointerEventData eventData) => _wallet.SubPoint();
}