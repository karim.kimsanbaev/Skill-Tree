﻿using System;
using UnityEngine.EventSystems;

public class BaseSkillNodeView : UIBehaviour, ISkillNodeView, IPointerClickHandler
{
    public string Id => Constants.BaseSkill;

    public void Init(Skills skills)
    {
    }

    public void MarkIsDirty()
    {
    }

    public event Action<string> Selected;

    public void OnPointerClick(PointerEventData eventData)
    {
        Selected?.Invoke(Id);
    }
}