﻿using System;

public interface ISkillNodeView
{
    string Id { get; }
    void Init(Skills skills);
    void MarkIsDirty();

    event Action<string> Selected;
}