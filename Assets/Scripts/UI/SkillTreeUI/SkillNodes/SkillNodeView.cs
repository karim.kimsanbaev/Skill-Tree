﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillNodeView : UIBehaviour, ISkillNodeView
{
    [SerializeField] private string _id;
    [SerializeField] private Toggle _toggle;

    [Header("Color Palette")] [SerializeField]
    private Color _learnedSkill;

    [SerializeField] private Color _allowedSkill;
    [SerializeField] private Color _notAllowedSkill;

    public event Action<string> Selected;

    public string Id => _id;
    private Skills _skills;

    private bool _isDirty;

    public void Init(Skills skills)
    {
        _skills = skills;
    }

    protected override void OnEnable()
    {
        _toggle.onValueChanged.AddListener(OnSelect);
    }

    protected override void OnDisable()
    {
        _toggle.onValueChanged.RemoveListener(OnSelect);
    }

    private void OnSelect(bool value)
    {
        if (!value)
        {
            return;
        }

        Debug.Log($"Select {_id}");
        Selected?.Invoke(_id);
    }

    public void MarkIsDirty()
    {
        _isDirty = true;
    }

    public void Update()
    {
        if (!_isDirty)
        {
            return;
        }

        _isDirty = false;

        var skill = _skills[_id];
        if (skill.IsLearned)
        {
            _toggle.targetGraphic.color = _learnedSkill;
            return;
        }

        var canLearn = _skills.CanLearn(_id);
        _toggle.targetGraphic.color = canLearn ? _allowedSkill : _notAllowedSkill;
    }

    public override string ToString()
    {
        return _id;
    }
}