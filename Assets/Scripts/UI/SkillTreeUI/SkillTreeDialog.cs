﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillTreeDialog : UIBehaviour
{
    [SerializeField] private SkillActionButtons _skillTreeButtons;
    [SerializeField] private SkillDescriptionView _skillDescriptionView;

#if UNITY_EDITOR
    [SerializeField] private string[] _nodesDebugView;
#endif

    private ISkillNodeView[] _nodes;

    private SkillTreePresenter _presenter;

    protected override void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Init(GameManager gameManager)
    {
        foreach (var node in _nodes)
        {
            node.Init(gameManager.Player.Skills);
        }

        _skillDescriptionView.Init(gameManager.References.SkillTree);

        _presenter = new SkillTreePresenter(_nodes, _skillTreeButtons, _skillDescriptionView, gameManager.Player);

        Show();
    }

    protected override void OnEnable() => _presenter?.OnEnable();

    protected override void OnDisable() => _presenter?.OnDisable();

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
        _nodes = GetComponentsInChildren<ISkillNodeView>();
        _nodesDebugView = _nodes.Select(node => node.ToString()).ToArray();
    }
#endif
}