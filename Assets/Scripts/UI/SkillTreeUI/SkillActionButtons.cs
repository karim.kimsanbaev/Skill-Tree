﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillActionButtons : UIBehaviour
{
    public event Action LearnClicked;
    public event Action ForgetClicked;
    public event Action ForgetAllSkillsClicked;

    [SerializeField] private Button _learnSkillButton;
    [SerializeField] private Button _forgetSkillButton;
    [SerializeField] private Button _forgetAllSkillsButton;

    protected override void Awake()
    {
        _learnSkillButton.interactable = false;
        _forgetSkillButton.interactable = false;
        _forgetAllSkillsButton.interactable = false;
    }

    protected override void OnEnable()
    {
        _learnSkillButton.onClick.AddListener(OnLearnClicked);
        _forgetSkillButton.onClick.AddListener(OnForgetClicked);
        _forgetAllSkillsButton.onClick.AddListener(OnForgetAllSkillsClicked);
    }

    protected override void OnDisable()
    {
        _learnSkillButton.onClick.RemoveListener(OnLearnClicked);
        _forgetSkillButton.onClick.RemoveListener(OnForgetClicked);
        _forgetAllSkillsButton.onClick.RemoveListener(OnForgetAllSkillsClicked);
    }

    private void OnLearnClicked()
    {
        LearnClicked?.Invoke();
    }

    private void OnForgetClicked()
    {
        ForgetClicked?.Invoke();
    }

    private void OnForgetAllSkillsClicked()
    {
        ForgetAllSkillsClicked?.Invoke();
    }

    public void SetLearnState()
    {
        _learnSkillButton.interactable = true;
        _forgetSkillButton.interactable = false;
        _forgetAllSkillsButton.interactable = true;
    }

    public void SetForgetState()
    {
        _learnSkillButton.interactable = false;
        _forgetSkillButton.interactable = true;
        _forgetAllSkillsButton.interactable = true;
    }

    public void SetNoneState()
    {
        _learnSkillButton.interactable = false;
        _forgetSkillButton.interactable = false;
        _forgetAllSkillsButton.interactable = true;
    }
}