﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

public class SkillTreePresenter
{
    private readonly SkillActionButtons _skillTreeButtons;
    private readonly SkillDescriptionView _skillDescriptionView;
    private readonly Dictionary<string, ISkillNodeView> _nodes;

    private readonly Player _player;

    [CanBeNull] private string _selectedSkillId;

    public SkillTreePresenter(ISkillNodeView[] nodes,
        SkillActionButtons skillActionButton,
        SkillDescriptionView skillDescriptionView, Player player)
    {
        _nodes = nodes.ToDictionary(node => node.Id, node => node);
        _skillTreeButtons = skillActionButton;
        _skillDescriptionView = skillDescriptionView;
        _player = player;
    }

    public void OnEnable()
    {
        foreach (var node in _nodes.Values)
        {
            node.Selected += OnSelected;
        }

        _skillTreeButtons.LearnClicked += LearnSelectedSkill;
        _skillTreeButtons.ForgetClicked += ForgetSelectedSkill;
        _skillTreeButtons.ForgetAllSkillsClicked += ForgetAllSkills;


        _player.SkillPoints.Changed += OnSkillPointsChanged;

        UpdateAllNodes();
    }

    public void OnDisable()
    {
        foreach (var node in _nodes.Values)
        {
            node.Selected -= OnSelected;
        }

        _skillTreeButtons.LearnClicked -= LearnSelectedSkill;
        _skillTreeButtons.ForgetClicked -= ForgetSelectedSkill;
        _skillTreeButtons.ForgetAllSkillsClicked -= ForgetAllSkills;

        _player.SkillPoints.Changed -= OnSkillPointsChanged;
    }

    private void OnSkillPointsChanged(SkillPoint total)
    {
        UpdateAllNodes();
        OnSelected(_selectedSkillId);
    }

    private void UpdateAllNodes()
    {
        foreach (var nodeId in _nodes.Keys)
        {
            UpdateNode(nodeId);
        }
    }

    private void UpdateNode(string id)
    {
        _nodes[id].MarkIsDirty();
    }

    private void OnSelected([CanBeNull] string id)
    {
        _selectedSkillId = id;
        SetDescription(id);
        SetActionForSkill(id);
    }

    private void SetDescription([CanBeNull] string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            return;
        }

        _skillDescriptionView.SetDescription(id);
    }

    private void SetActionForSkill([CanBeNull] string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            return;
        }

        if (_player.Skills.CanLearn(id))
        {
            _skillTreeButtons.SetLearnState();
            return;
        }

        if (_player.Skills.CanForget(id))
        {
            _skillTreeButtons.SetForgetState();
            return;
        }

        _skillTreeButtons.SetNoneState();
    }

    private void LearnSelectedSkill()
    {
        _player.Skills.Learn(_selectedSkillId);

        UpdateAllNodes();
        OnSelected(_selectedSkillId);
    }

    private void ForgetSelectedSkill()
    {
        _player.Skills.Forget(_selectedSkillId);

        UpdateAllNodes();
        OnSelected(_selectedSkillId);
    }

    private void ForgetAllSkills()
    {
        _player.Skills.ForgetAll();

        UpdateAllNodes();
        OnSelected(_selectedSkillId);
    }
}