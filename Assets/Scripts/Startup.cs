﻿using System;
using System.Collections;
using UnityEngine;

public class Startup : MonoBehaviour
{
    [SerializeField] private UIDialogs _uiDialogs;

    private GameManager _gameManager;

    public IEnumerator Start()
    {
        _gameManager = new GameManager(_uiDialogs);
        yield return _gameManager.InitAsync();
    }

    private void OnApplicationQuit()
    {
        _gameManager.Dispose();
    }
}